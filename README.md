# Wallabag Install Script

I was annoyed with the clunky install instructions / two seperate pages for Wallabag. I wrote a script to automate that process.

# Usage

`curl -s https://git.coolaj86.com/josh/wallabag-install/raw/branch/master/install.sh | bash`
