# Wallabag Install Script v1.1a by Joshua Mudge
# Ad Mejorem Dei Glorium

sudo apt-get install nginx php php-curl php-dom php-gd php-xml php-bcmath php-mbstring php-fpm php7.0-sqlite3
cd /var/www/
sudo git clone https://github.com/wallabag/wallabag.git
sudo chown -R $USER:$USER /var/www/wallabag
cd wallabag
curl -s https://getcomposer.org/installer | php
make install
sudo chown www-data:www-data /var/www/wallabag -R

# You also need to install a database server, which it didn't tell you. Still need to automate parameters.yml
# %kernel.cache_dir%/data.db
# https://doc.wallabag.org/en/admin/parameters.html
# Use pdo_sqlite for driver and %kernel.cache_dir%/data.db for database path.
# You can't use subdomains for the install.
# Make sure to specify https:// in the URL.


# Have script auto-fetch Nginx config and symlink.
